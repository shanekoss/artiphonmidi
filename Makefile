
PLATFLAG :=
ifeq ($(OS),Windows_NT)
	PLATFLAG = Windows
else
	UNAME_S := $(shell uname -s)
	ifeq ($(UNAME_S),Darwin)
		PLATFLAG = macOS
	endif
endif

TARGET_JUCE_VERSION = 6.0.8

ifeq ($(PLATFLAG),macOS)
#osx

JUCE_BUILD = ext/JUCE/extras/Projucer/Builds/MacOSX/build/Release/Projucer.app
JUCER = /Applications/Projucer/$(TARGET_JUCE_VERSION)/Projucer.app/Contents/MacOS/Projucer
JUCER_APP = /Applications/Projucer/$(TARGET_JUCE_VERSION)/Projucer.app
$(eval JUCER_APP_VERSION=$(shell mdls -name kMDItemVersion -raw $(JUCER_APP)))
JUCER_XCODEPROJECT = $(shell pwd)/ext/JUCE/extras/Projucer/Builds/MacOSX/Projucer.xcodeproj

projucer:
ifeq ($(JUCER_APP_VERSION), $(TARGET_JUCE_VERSION))
	@echo "JUCE CURRENT, NO NEED TO BUILD"
else
	sudo xcode-select -s /Applications/Xcode.app/Contents/Developer/
	xcodebuild -project $(JUCER_XCODEPROJECT) -configuration Release clean > /dev/null
	xcodebuild -project $(JUCER_XCODEPROJECT) -configuration Release > /dev/null
	sudo ditto $(JUCE_BUILD) /Applications/Projucer/$(TARGET_JUCE_VERSION)/Projucer.app
endif

test:
	echo fudge

artiphon_midi_demo:
	$(JUCER) --resave MidiDemo/MidiDemo.jucer
	xcodebuild -project MidiDemo/Builds/MacOSX/MidiDemo.xcodeproj -configuration Release clean > /dev/null
	xcodebuild -project MidiDemo/Builds/MacOSX/MidiDemo.xcodeproj -target "MidiDemo - App" -configuration Release > /dev/null

endif


ifeq ($(PLATFLAG),Windows)
#windows

endif
