/*
  ==============================================================================

    MidiSelectButton.h
    Created: 23 Mar 2021 6:18:48pm
    Author:  Shane Koss

  ==============================================================================
*/

#pragma once
#include <JuceHeader.h>

class MidiSelectButton : public Button
{
public:
    MidiSelectButton(const String buttonType, const String textWhenOff, const String textWhenOn);
    
    ~MidiSelectButton();
    
    void paintButton (Graphics& g, bool shouldDrawButtonAsHighlighted, bool shouldDrawButtonAsDown) override;
    
private:
    const String offText;
    const String onText;
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MidiSelectButton)
};
