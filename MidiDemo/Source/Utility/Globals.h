/*
  ==============================================================================

    Globals.h
    Created: 23 Mar 2021 6:27:00pm
    Author:  Shane Koss

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

namespace Artiphon {

namespace IDs {
    #define DECLARE_ID(name) const juce::Identifier name (#name);
    /* an indentifier representing a boolean value in a value tree to pass or filter out Note On data */
    DECLARE_ID( NoteOn )
    /* an indentifier representing a boolean value in a value tree to pass or filter out Note Off data */
    DECLARE_ID( NoteOff )
    /* an indentifier representing a boolean value in a value tree to pass or filter out Midi CC data */
    DECLARE_ID( MidiCC )
    /* an indentifier representing a boolean value in a value tree to pass or filter out Midi Sysex data */
    DECLARE_ID( Sysex )

    /* an indentifier for the value tree store root type  */
    DECLARE_ID( store )
    /* an indentifier for the inputs child value tree of the store root tree  */
    DECLARE_ID( inputs )
    /* an indentifier for the outputs child value tree of the store root tree  */
    DECLARE_ID( outputs )
} /* end IDs */

} /* end Artiphon */
