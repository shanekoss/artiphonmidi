/*
  ==============================================================================

    MidiSelectButton.cpp
    Created: 23 Mar 2021 6:18:48pm
    Author:  Shane Koss

  ==============================================================================
*/

#include "MidiSelectButton.h"

MidiSelectButton::MidiSelectButton(const String buttonType, const String textWhenOff, const String textWhenOn) : Button(buttonType), offText(textWhenOff), onText( textWhenOn )
{
    
}

MidiSelectButton::~MidiSelectButton()
{
    
}

void MidiSelectButton::paintButton (Graphics& g, bool shouldDrawButtonAsHighlighted, bool shouldDrawButtonAsDown)
{
    g.fillAll( getToggleState() ? Colours::indianred : Colours::lightgrey );
    g.setColour( getToggleState() ? Colours::white : Colours::black );
    g.drawText( getToggleState() ? onText : offText , getLocalBounds(), Justification::centred );
    g.setColour( Colours::darkblue );
    g.drawRect( getLocalBounds() );
}
